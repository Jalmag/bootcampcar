﻿namespace Model
{
    public class Car
    {
        // Some constants to make the car handling feel somewhat real-like
        private const float MASS = 15;
        private const float HORSEPOWER = 200;
        private const float FRICTION = 40;
        private const float AIR_RESISTANCE = 0.005f;
        private const float ROLLING_RESISTANCE = 10f;
        private const float MINIMUM_V_TO_TURN = 1;

        private float velocity = 0;
        private float direction = 0; // Direction in degrees, 0 being north and 180 south
        private bool engineIsOn = false;

        public float Turn(float change, float time)
        {
            // Cars don't turn if they are not moving
            if (velocity > MINIMUM_V_TO_TURN)
            {
                direction += change * time;
            }
            // To keep direction between 0 and 360
            direction = (direction + 360) % 360;
            return direction;
        }

        public float GetDirection()
        {
            return direction;
        }

        public bool GetIsEngineOn()
        {
            return engineIsOn;
        }

        // Turn the engine on and off
        public bool ToggleEngine(bool newState)
        {
            engineIsOn = newState;
            return engineIsOn;
        }

        public float BrakeOrAccelerate(float change, float time)
        {
            // Braking (brakes work even if engine is off)
            if (change < 0)
            {
                float deceleration = FRICTION;
                velocity += change * deceleration * time;
                if (velocity < 0)
                {
                    velocity = 0;
                }
            }
            // Applying throttle
            else
            {
                if (engineIsOn)
                {
                    float acceleration = HORSEPOWER / MASS;
                    velocity += change * acceleration * time;
                }
            }
            return velocity;
        }

        // Without throttle car will eventually stop
        public float ApplyAirResistance(float time)
        {
            float change = (AIR_RESISTANCE * velocity * velocity + ROLLING_RESISTANCE) / MASS * time;
            velocity += -change;
            if (velocity < 0)
            {
                velocity = 0;
            }
            return velocity;
        }
    }
}