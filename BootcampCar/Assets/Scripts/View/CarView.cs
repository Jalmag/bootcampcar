﻿using Controller;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class CarView : MonoBehaviour
    {
        public CarController controller;
        public Text velocityText;
        public Text directionText;
        public Text engineOnText;
        public Text throttleText;
        public Text steeringText;
        public Slider throttle;
        public Slider steering;
        public Transform carIcon;
        public GameObject BGImage1;
        public GameObject BGImage2;

        public void Update()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                throttle.value += 0.01f;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                throttle.value += -0.01f;
            }

            controller.BrakeOrAccelerate(throttle.value);
            controller.Turn(5 *steering.value);
        }

        public void UpdateView(float velocity, float direction)
        {
            // Formatting to 1 decimal places
            velocityText.text = velocity.ToString("F1") + " km/h";
            directionText.text = direction.ToString("F1");
            carIcon.eulerAngles = new Vector3(0, 0,-direction);

            // Scrolling background
            BGImage1.transform.position += new Vector3(0, 0.15f* - velocity * Time.deltaTime, 0);
            BGImage2.transform.position += new Vector3(0, 0.15f* -velocity * Time.deltaTime, 0);
            if (BGImage2.transform.position.y < 0)
            {
                BGImage1.transform.position = new Vector3(0, 0, 0);
                BGImage2.transform.position = new Vector3(0, 21.5f, 0);
            }
            Camera.main.transform.eulerAngles = new Vector3(0, 0, direction);
        }

        public void UpdateEngine(bool engineState)
        {
            if (engineState)
            {
                engineOnText.text = "PÄÄLLÄ";
                engineOnText.color = Color.green;
            }
            else
            {
                engineOnText.text = "POIS PÄÄLTÄ";
                engineOnText.color = Color.red;
            }
        }

        // Callback functions for UI components
        public void ThrottleChanged(float value)
        {
            throttleText.text = (100 * value).ToString("F0") + " %";
            if (value < 0)
            {
                throttleText.color = Color.red;
            }
            else
            {
                throttleText.color = Color.green;
            }
        }

        public void SteeringChanged(float value)
        {
            steeringText.text = value.ToString("F1");
        }

        public void EngineToggled(bool value)
        {
            controller.ToggleEngine(value);
        }
    }
}