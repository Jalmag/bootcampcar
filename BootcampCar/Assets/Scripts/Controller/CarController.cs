﻿using Model;
using View;
using UnityEngine;

namespace Controller
{
    public class CarController : MonoBehaviour
    {
        public CarView view;
        private Car car;

        // Use this for initialization
        void Start()
        {
            car = new Car();
            view.UpdateEngine(car.GetIsEngineOn());
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            float velocity = car.ApplyAirResistance(Time.deltaTime);
            float direction = car.GetDirection();
            view.UpdateView(velocity, direction);
        }

        public void Turn(float change)
        {
            if (change != 0)
            {
                car.Turn(change, Time.deltaTime);
            }   
        }

        public void BrakeOrAccelerate(float change)
        {
            if (change != 0)
            {
                car.BrakeOrAccelerate(change, Time.deltaTime);
            }
        }

        public void ToggleEngine(bool isEngineOn)
        {
            car.ToggleEngine(isEngineOn);
            view.UpdateEngine(car.GetIsEngineOn());
        }
    }
}